package com.aaron.equiposalazar;

import android.app.Application;

/**
 * Created by Aaron on 19/09/2017.
 */

public class MyApp extends Application {
    GestorBasedeDatos gestorBD;
    BaseDatos base;
    @Override
    public void onCreate() {
        super.onCreate();
        base= new BaseDatos(getApplicationContext(),BaseDatos.DB_NAME,null,1);
        gestorBD = new GestorBasedeDatos(getApplicationContext(),base);

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        base.close();
    }

    public GestorBasedeDatos getGestorBD() {
        return gestorBD;
    }
}

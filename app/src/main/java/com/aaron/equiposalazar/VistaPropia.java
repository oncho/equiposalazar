package com.aaron.equiposalazar;

import android.annotation.TargetApi;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Administrador on 10/07/2015.
 */
public class VistaPropia extends LinearLayout {

    private TextView text;
    private ImageView img;

    public VistaPropia(Context context) {
        super(context, null);
        addElements(context);
    }


    public VistaPropia(Context context, AttributeSet attrs) {
        super(context, attrs);
        addElements(context);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public VistaPropia(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        addElements(context);

    }


    private void addElements(Context context){
        String infService= Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li= (LayoutInflater) getContext().getSystemService(infService);
        li.inflate(R.layout.vista_propia,this,true);
        text= (TextView) findViewById(R.id.text_nombre);
        img= (ImageView) findViewById(R.id.foto);

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT,
                1.0f
        );
        this.setLayoutParams(param);
    }

    public void setText(String txt){
        this.text.setText(txt);
    }

    public void setTextColor(int color){this.text.setTextColor(color);}

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void setImage(String ruta){
        Bitmap bitmap=BitmapFactory.decodeFile(ruta);
        if(bitmap==null){
            BitmapDrawable bitmapDraw= (BitmapDrawable) img.getDrawable();
            if(bitmapDraw==null){
                img.buildDrawingCache();
                bitmap = img.getDrawingCache();
                img.buildDrawingCache(false);
            }else
            {
                bitmap = bitmapDraw .getBitmap();

            }
        }

        Bitmap bitmaprounded= EditaIMG.getRoundedCornerBitmap(bitmap);
        bitmaprounded= EditaIMG.redimensionarImagenMaximo(bitmaprounded,80,80);
        img.setImageBitmap(bitmaprounded);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void setColor(int id){
        Resources res= getResources();
        Drawable verde= res.getDrawable(R.drawable.borde_verde);
        Drawable blanco=res.getDrawable(R.drawable.borde);
        switch (id){
            case 1: this.img.setBackground(verde);break;
            case 2: this.img.setBackground(blanco);break;
                default:
        }

    }
}

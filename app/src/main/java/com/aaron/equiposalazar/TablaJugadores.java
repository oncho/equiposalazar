package com.aaron.equiposalazar;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

/**
 * Created by Aaron on 18/09/2017.
 */

class TablaJugadores {
    private final static String NAME="jugadores";

    private SQLiteDatabase sqlDb;

    TablaJugadores(SQLiteDatabase sqlDb){
        this.sqlDb=sqlDb;
    }

    public TablaJugadores() {

    }

    public void setSqlDb(SQLiteDatabase sqlDb) {
        this.sqlDb = sqlDb;
    }

    public class Columns implements BaseColumns {
        public final static String ID = "id";
        public final static String NOMBRE = "nombre";
        public final static String FOTO = "foto";
    }

    private final static String[] COLUMNS = {
            TablaJugadores.Columns.ID, Columns.NOMBRE, Columns.FOTO
    };

    public static final String CREATE_TABLE= "CREATE TABLE IF NOT EXISTS "+NAME+
            " ("+ TablaJugadores.Columns.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"+
             Columns.NOMBRE + " TEXT NOT NULL,"+
             Columns.FOTO + " TEXT )";

    public int insert(String nombre,String  foto ){
        ContentValues values= new ContentValues();
        values.put(Columns.NOMBRE,nombre);
        values.put(Columns.FOTO,foto);
        int id=0;
        if(sqlDb.isOpen()){
            if(sqlDb.insert(NAME,null,values)>0){
                Cursor c= sqlDb.rawQuery("SELECT LAST_INSERT_ROWID()",null);
                if(c.moveToFirst()){
                    while(c.moveToNext()){
                        id=c.getInt(0);
                    }
                }
            }
        }

        return id ;
    }

    public boolean update(int id,String nombre,String  foto ){
        String args[]= new String[1];
        args[0]=""+id;
        boolean okUpdate=false;
        ContentValues values= new ContentValues();
        values.put(Columns.NOMBRE,nombre);
        values.put(Columns.FOTO,foto);
        if(sqlDb.update(NAME,values,"id=?",args)>0)
            okUpdate=true;
        return okUpdate;
    }


    public boolean delete(int id){
        String args[]= new String[1];
        args[0]=""+id;
        boolean okdelete=false;
        if(sqlDb.delete(NAME,"id=?",args)>0)
            okdelete=true;
        return okdelete;
    }

    public static String getNAME() {
        return NAME;
    }

    public static String[] getCOLUMNS() {
        return COLUMNS;
    }

    public boolean isEmpty(){
        boolean emptyTable= false;
        if(sqlDb.query(NAME,COLUMNS,null,null,null,null,null).getCount()==0){
            emptyTable=true;
        }
        return emptyTable;
    }


}

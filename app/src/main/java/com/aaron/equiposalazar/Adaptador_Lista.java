package com.aaron.equiposalazar;

import java.util.List;


import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;

public class Adaptador_Lista extends ArrayAdapter<Jugador> {
	
	private LayoutInflater inflador;
	private List <Jugador> jugadores;

	public Adaptador_Lista(Context context, int resource, List<Jugador> objects) {
		super(context, resource, objects);
		inflador= LayoutInflater.from(context);
		jugadores= objects;
	}
	
	
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView==null){
			convertView= inflador.inflate(R.layout.elemento_lista, null);
			
			holder = new ViewHolder();
			
			holder.imagen= (ImageView) convertView.findViewById(R.id.imageView1);
			holder.check=(CheckBox) convertView.findViewById(R.id.checkBox1);
			
			convertView.setTag(holder);
		}else {
			holder= (ViewHolder) convertView.getTag();
		}
		final Jugador jugador= getItem(position);
		holder.imagen.setTag(jugador.getFoto());
		holder.check.setText(jugador.getFoto());
		holder.check.setChecked(jugador.isChequeado());
		
		ImageView laimagen= (ImageView) convertView.findViewById(R.id.imageView1);
		Bitmap bitmap=null;
		if(null==jugador.getFoto()){
			BitmapDrawable bitmapDraw= (BitmapDrawable) laimagen.getDrawable();
			if(bitmapDraw==null){
				laimagen.buildDrawingCache();
				bitmap = laimagen.getDrawingCache();
				laimagen.buildDrawingCache(false);
			}else
			{
				bitmap = bitmapDraw .getBitmap();
			}
		}else {
			bitmap = EditaIMG.rotateImage(jugador.getFoto());
			if(null!=bitmap)
			bitmap = EditaIMG.getRoundedCornerBitmap(bitmap);
		}


			laimagen.setImageBitmap(bitmap);


		
		
		
		CheckBox check = (CheckBox) convertView.findViewById(R.id.checkBox1);
		check.setText(jugador.getNombre());
		check.setChecked(jugador.isChequeado());
		check.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				CheckBox check= (CheckBox) v;
				if(jugador.isChequeado()){
					jugador.setChequeado(false);
					check.setChecked(jugador.isChequeado());
				}
				else{
					jugador.setChequeado(true);
					check.setChecked(jugador.isChequeado());
				}
				Principal.contarJugadores();
				
			}
		});

		Drawable siSeleccionado = getContext().getResources().getDrawable(R.drawable.fondo_selected);
		Drawable noSeleccionado = getContext().getResources().getDrawable(R.drawable.fondo_no_selected);
		int SDK = Build.VERSION.SDK_INT;
		if(SDK < Build.VERSION_CODES.JELLY_BEAN){
			if (jugador.isSelecionado()) {
				convertView.setBackgroundDrawable(siSeleccionado);
			} else {
				convertView.setBackgroundDrawable(noSeleccionado);
			}
		}else {
			if (jugador.isSelecionado()) {
				convertView.setBackground(siSeleccionado);
			} else {
				convertView.setBackground(noSeleccionado);
			}
		}
		return convertView;
	}
static class ViewHolder{
	ImageView imagen;
	CheckBox check;
	
}


}

package com.aaron.equiposalazar;
import java.util.ArrayList;
/**
 * Created by Administrador on 09/07/2015.
 */
public class Comunicador {
    private static ArrayList<Jugador> array=null;
    private static Jugador jugador=null;

    public static Jugador getJugador() {
        return jugador;
    }

    public static void setJugador(Jugador jugador) {
        Comunicador.jugador = jugador;
    }

    public static void setArray(ArrayList<Jugador> nuevoArray){
        array=nuevoArray;
    }
    public static ArrayList<Jugador> getArray(){
        return array;
    }

}

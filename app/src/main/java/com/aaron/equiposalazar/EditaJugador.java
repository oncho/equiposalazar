package com.aaron.equiposalazar;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EditaJugador extends Activity {

    private static final int TOMAR_FOTO=1;

    private static final  int USAR_GALERIA=2;

    private int opcion=0;
    //ruta base de la SDcard: /storage/sdcard0/...
    private final String ruta_fotos = Environment.getExternalStorageDirectory().
            getAbsolutePath() +"/equipoalazar/com/imagenes/";
    private File file = new File(ruta_fotos);

    private File mi_foto;
    private Button btn_add;
    private ImageView img_foto;
    private EditText txt_nombre;

    private Jugador jugador;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edita_jugador);

        btn_add= (Button)findViewById(R.id.btn_add);
        img_foto= (ImageView)findViewById(R.id.img_foto);
        txt_nombre=(EditText) findViewById(R.id.txt_nombre);
        if(Comunicador.getJugador()!=null){
            jugador= Comunicador.getJugador();
            img_foto.setImageBitmap(BitmapFactory.decodeFile(jugador.getFoto()));
            txt_nombre.setText(jugador.getNombre());
        }
        file.mkdirs();

    }

    public void agregarJugador(View view){
        String mensaje="Nombre no valido";
        String rutaImagen=img_foto.getContentDescription().toString();
        if(txt_nombre.getText().length()<2){
            Toast toast = Toast.makeText(this,mensaje, Toast.LENGTH_SHORT);
            toast.show();
        }else{
            if(Comunicador.getJugador()!=null){
                Comunicador.getJugador().setFoto(rutaImagen);
                Comunicador.getJugador().setNombre(txt_nombre.getText().toString());

            }else{
                Jugador newJugador=new Jugador();
                newJugador.setNombre(txt_nombre.getText().toString());
                newJugador.setFoto(rutaImagen);
                Comunicador.setJugador(newJugador);
            }
            setResult(RESULT_OK);
            finish();
        }

    }
    public void opcionFoto(View view){
        final DialogoCaptura dialogo = new DialogoCaptura(this,opcion);
        RadioButton rb_foto = (RadioButton) dialogo.findViewById(R.id.rb_foto);
        RadioButton rb_galeria = (RadioButton) dialogo.findViewById(R.id.rb_galeria);

        rb_foto.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                tomarFoto(TOMAR_FOTO);
                dialogo.dismiss();
            }
        });
        rb_galeria.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                tomarFoto(USAR_GALERIA);
                dialogo.dismiss();
            }
        });
        dialogo.show();


    }

    public void tomarFoto(int action){
        String file = ruta_fotos + getCode() + ".jpg";
        mi_foto = new File( file );
        try {
            mi_foto.createNewFile();
        } catch (IOException ex) {
            Log.e("ERROR ", "Error:" + ex);
        }
        //ruta para guardar la foto
        Uri uri = Uri.fromFile( mi_foto );
        Intent cameraIntent =null;
        int opcion=0;
        switch (action){
            case TOMAR_FOTO:
                //Abre la camara para tomar la foto
                cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                opcion=TOMAR_FOTO;
                break;
            case USAR_GALERIA:
                cameraIntent= new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                opcion=USAR_GALERIA;
                break;
        }
        //Guarda imagen
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        //Retorna a la actividad
        startActivityForResult(cameraIntent, opcion);

    }

    /**
     * Metodo privado que genera un codigo unico segun la hora y fecha del sistema
     * * @return photoCode
     * */
    private String getCode() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
        String date = dateFormat.format(new Date() );
        String photoCode = "pic_" + date;
        return photoCode;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Comprobamos si el resultado de la segunda actividad es "RESULT_CANCELED".
        if (resultCode == RESULT_CANCELED) {
            // Si es así mostramos mensaje de cancelado por pantalla.
            Toast.makeText(this, "Resultado cancelado", Toast.LENGTH_SHORT)
                    .show();
        } else {
            final Bitmap[] bitmap = {null};
            if(requestCode==TOMAR_FOTO) {

                 bitmap[0] =BitmapFactory.decodeFile(mi_foto.getPath());

                 bitmap[0] = EditaIMG.rotateImage(mi_foto.getPath());

                 img_foto.setContentDescription(mi_foto.getPath());
            }
            if(requestCode==USAR_GALERIA){
                final Uri selectedImage = data.getData();
                final InputStream[] is = new InputStream[1];
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            is[0] = getContentResolver().openInputStream(selectedImage);
                            //BufferedInputStream bis = new BufferedInputStream(is);
                            OutputStream out = new FileOutputStream(mi_foto);
                            byte[] buf = new byte[2048];
                            int len;
                            while ((len = is[0].read(buf)) > 0) {
                                out.write(buf, 0, len);
                            }
                            is[0].close();
                            out.close();
                            bitmap[0] =BitmapFactory.decodeFile(mi_foto.getPath());
                            img_foto.setContentDescription(mi_foto.getPath());
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();

            }
            img_foto.setImageBitmap(bitmap[0]);

        }
    }
}

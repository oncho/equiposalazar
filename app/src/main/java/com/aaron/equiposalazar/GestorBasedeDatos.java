package com.aaron.equiposalazar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrador on 29/06/2015.
 */
public class GestorBasedeDatos {
    private  BaseDatos miBase;

    private SQLiteDatabase bd;


    public GestorBasedeDatos(Context context,BaseDatos base){
        miBase=base;

    }

    public Jugador insertarJugadores(Jugador jugador){
        ContentValues values= new ContentValues();
        values.put(TablaJugadores.Columns.NOMBRE,jugador.getNombre());
        values.put(TablaJugadores.Columns.FOTO,jugador.getFoto());
        int id=0;
        if(miBase.getWritableDatabase().isOpen()){
            if(miBase.getWritableDatabase().insert(TablaJugadores.getNAME(),null,values)>0){
                Cursor c=miBase.getReadableDatabase().rawQuery("SELECT last_insert_rowid()",null);
                if(c.moveToFirst()){
                    id=c.getInt(0);
                }
            }
        }
        jugador.setCod(id);
        return jugador;
    }
    public boolean eliminarJugador(int id){
        boolean ok=false;
        //abrimos la base de datos en modo lectura y escritura
        bd = miBase.getWritableDatabase();
        //si la bd esta abierta
        if(bd.isOpen()){
            String args[]= new String[1];
            args[0]=""+id;
            ok=(bd.delete(TablaJugadores.getNAME(),"id=?",args)>0);
        }
        bd.close();
        return ok;
    }

    public boolean actualizarJugador(int id,Jugador jugador){
        boolean okUpdate=false;
        //abrimos la base de datos en modo lectura y escritura
        bd = miBase.getWritableDatabase();
        //si la bd esta abierta
        if(bd.isOpen()){
            String args[]= new String[1];
            args[0]=""+id;

            ContentValues values= new ContentValues();
            values.put(TablaJugadores.Columns.NOMBRE,jugador.getNombre());
            values.put(TablaJugadores.Columns.FOTO,jugador.getFoto());
            if(bd.update(TablaJugadores.getNAME(),values,"id=?",args)>0)
                okUpdate=true;
            return okUpdate;
        }
        bd.close();
        return okUpdate;
    }

    public List<Jugador> verJugadores(List<Jugador> jugadores){
        bd = miBase.getReadableDatabase();
        //creo el objeto Cursor
        Cursor cur= bd.rawQuery("SELECT id,nombre,foto FROM JUGADORES",null);
        //recogemos los datos en un array de string
        jugadores= new ArrayList<Jugador>();
        if(cur.moveToFirst()){
            int i=0;
            do{
                Jugador jugador= new Jugador();
                //recojo los datos de la base de datos
                //abrimos la base de datos en modo lectura
                jugador.setCod(cur.getInt(0));
                jugador.setNombre(cur.getString(1));
                jugador.setFoto(cur.getString(2));
                jugadores.add(i,jugador);
                i++;
            }while(cur.moveToNext());
        }
        bd.close();
        return jugadores;
    }



   public void modificarJugador(Jugador jugador,int id){
        //abrimos la base de datos en modo lectura y escritura
        SQLiteDatabase bd = miBase.getWritableDatabase();
        //si la bd esta abierta
        if(bd.isOpen()){
            bd.execSQL("UPDATE JUGADORES SET nombre="+jugador.getNombre()+",foto='"+
                   jugador.getFoto()+"' WHERE codigo="+id);
       }
        bd.close();
  }

    public void close() {
        miBase.close();
    }
}

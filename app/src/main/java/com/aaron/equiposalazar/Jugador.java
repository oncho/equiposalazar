package com.aaron.equiposalazar;

public class Jugador {
	private int cod;
	
	private String nombre;
	
	private String foto;

	public Jugador(){
		this.cod=0;
	}
	private boolean selecionado;

	public int getCod() {
		return cod;
	}

	public void setCod(int cod) {
		this.cod = cod;
	}

	public boolean isSelecionado() {
		return selecionado;
	}


	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

	private boolean chequeado;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public boolean isChequeado() {
		return chequeado;
	}
	public void setChequeado(boolean chequeado) {
		this.chequeado = chequeado;
	}
	
	

}

package com.aaron.equiposalazar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


public class Principal extends Activity {

	private final static int EDITAR=1;
	private final static int NUEVO=2;
	private ListView lista;
	
	private static TextView n_jug;
	
	private static List<Jugador> jugadores;
	
	private static Spinner spinner;
	
	private static Adaptador_Lista adaptador;
	final Context context= this;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_principal);
		final MyApp myApp= (MyApp)getApplication();
		jugadores=new ArrayList<Jugador>();

		jugadores= myApp.getGestorBD().verJugadores(jugadores);



		lista= (ListView) findViewById(R.id.listView1);
		n_jug= (TextView) findViewById(R.id.textView1);
		spinner= (Spinner) findViewById(R.id.spinner1);
		adaptador = new Adaptador_Lista(this, R.layout.elemento_lista, jugadores);
		contarJugadores();
		cantidadEquipos();

		ImageButton botonAdd= (ImageButton) findViewById(R.id.botonAdd);
		ImageButton botonDel= (ImageButton) findViewById(R.id.botonDel);
		Button botonEquipos= (Button) findViewById(R.id.boton_Equipos);
		
		botonAdd.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getApplicationContext(),EditaJugador.class);
                Comunicador.setJugador(null);
				startActivityForResult(intent,NUEVO);
			}
		});
		botonDel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				for(int i=0;i<adaptador.getCount();i++){
					Jugador j= adaptador.getItem(i);
					if(j.isSelecionado()){
						myApp.getGestorBD().eliminarJugador(j.getCod());
						mostrarMensaje(j.getNombre() + " eliminado");
						adaptador.remove(j);

					}
				}
				contarJugadores();
				
			}
		});
		botonEquipos.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				ArrayList<Jugador> participantes = generarEquipos();
				Comunicador.setArray(participantes);
				Intent intent= new Intent(Principal.this,Secundaria.class);
				startActivity(intent);
			}
		});
		adaptador= new Adaptador_Lista(this,R.layout.elemento_lista,jugadores);
		lista.setAdapter(adaptador);
		lista.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		lista.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> a, View v, int position,
					long id) {
				    for(int i=0; i<adaptador.getCount();i++){
						Jugador jugador= (Jugador) lista.getAdapter().getItem(i);
						Jugador jugadorSelecionado= (Jugador) lista.getAdapter().getItem(position);
						if(jugador==jugadorSelecionado){
							jugador.setSelecionado(true);
						}else{
							jugador.setSelecionado(false);
						}
					}
					adaptador.notifyDataSetChanged();
			}
		});

		lista.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
				Jugador jugador = (Jugador) lista.getAdapter().getItem(i);
				Comunicador.setJugador(jugador);
				Intent intent = new Intent(Principal.this,EditaJugador.class);
                startActivityForResult(intent,EDITAR);
				return true;
			}

		});

		
	}




	protected void mostrarMensaje(String mensaje) {
		Toast toast = Toast.makeText(this,mensaje, Toast.LENGTH_SHORT);
		toast.show();
		
	}

	static void contarJugadores() {
		int seleccionados=0;
		if(!jugadores.isEmpty()){
			for( Jugador jugador: jugadores){
				if(jugador.isChequeado()){
					seleccionados++;
				}
			}
			n_jug.setText("Jugadores: \n" + seleccionados + "/"+adaptador.getCount());
		}
	}
	public void addJugador(String nombre,String rutaImagen){
		Jugador jugador= new Jugador();
		jugador.setNombre(nombre);
        jugador.setFoto(rutaImagen);
		jugador=((MyApp)getApplication()).getGestorBD().insertarJugadores(jugador);
		if(jugador.getCod()>0){
            adaptador.add(jugador);
            adaptador.notifyDataSetChanged();
            contarJugadores();
		}
	}

	private  void cantidadEquipos() {
		if(adaptador.getCount()>2){
			String[] adaptador = new String[((jugadores.size()/2))];
			for(int i=0;i<adaptador.length;i++){
				if(i==0){
					adaptador[i]=""+2;
				}else{
					adaptador[i]=""+(i+2);
				}
			}
			ArrayAdapter<String> modelo= 
					new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,adaptador);
			
			spinner.setAdapter(modelo);
		}
		
	}
	private ArrayList<Jugador> generarEquipos(){
		ArrayList <Jugador> participantes = new ArrayList<Jugador>();
		for(int i=0;i<adaptador.getCount();i++)
			if (adaptador.getItem(i).isChequeado()) {
				Jugador participante = adaptador.getItem(i);
				participantes.add(participante);
			}
		Random rd= new Random();
		Collections.shuffle(participantes,rd);
		return participantes;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.principal, menu);
		return true;
	}

    public  void updateJugador(int cod, String nombre, String foto) {
		for(int i =0;i<adaptador.getCount();i++){
			if(adaptador.getItem(i).getCod()==cod){
				adaptador.getItem(i).setNombre(nombre);
				adaptador.getItem(i).setFoto(foto);
				Jugador jugador=(Jugador)adaptador.getItem(i);
				if(((MyApp)getApplication()).getGestorBD().actualizarJugador(cod,jugador)){
					adaptador.notifyDataSetChanged();
				};
			}

		}

    }

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_CANCELED) {
			// Si es así mostramos mensaje de cancelado por pantalla.
			Toast.makeText(this, "Resultado cancelado", Toast.LENGTH_SHORT)
					.show();
		} else {
			Jugador j=Comunicador.getJugador();
			if(requestCode==EDITAR) {
				updateJugador(j.getCod(),j.getNombre(),j.getFoto());
			}
			if(requestCode==NUEVO){
				addJugador(j.getNombre(),j.getFoto());
			}

		}
	}
}

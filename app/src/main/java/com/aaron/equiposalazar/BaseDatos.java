package com.aaron.equiposalazar;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Administrador on 29/06/2015.
 */
public class BaseDatos extends SQLiteOpenHelper {

    final static int VERSION=1;

    final static String DB_NAME="miembros";

    private Context context;

    TablaJugadores tablajugadores;

    public BaseDatos(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        onCreate(this.getWritableDatabase());
        tablajugadores= new TablaJugadores(this.getWritableDatabase());
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        createTables(sqLiteDatabase);


    }

    private void createTables(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(TablaJugadores.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TablaJugadores.getNAME());
        sqLiteDatabase.execSQL(TablaJugadores.CREATE_TABLE);
    }

    public TablaJugadores getTablajugadores() {
        return tablajugadores;
    }
}

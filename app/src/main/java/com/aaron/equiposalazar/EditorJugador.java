package com.aaron.equiposalazar;

import android.app.AlertDialog;
import android.app.Dialog;

import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created by Administrador on 01/07/2015.
 */
public class EditorJugador extends Dialog {
    protected ImageView imagen;
    protected EditText nombre;
    protected Button boton;

    public EditorJugador(final Context context) {
        super(context);
        setTitle("Nuevo Jugador");
        setContentView(R.layout.editor_jugadores);
        imagen= (ImageView) findViewById(R.id.imageView_edit);
        imagen.setImageResource(R.drawable.ic_launcher);
        nombre= (EditText) findViewById(R.id.editText_nombre);
        boton= (Button) findViewById(R.id.button_add_jug);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mensaje="Nombre no valido";
                if(nombre.getText().length()<2){
                    Toast toast = Toast.makeText(context,mensaje, Toast.LENGTH_SHORT);
                    toast.show();
                }else{
                    //Principal.addJugador(nombre.getText().toString(), "");
                    dismiss();
                }


            }
        });
    }
}

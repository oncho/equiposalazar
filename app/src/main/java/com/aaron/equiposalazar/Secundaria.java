package com.aaron.equiposalazar;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;

public class Secundaria extends Activity {
    //private TextView etiqueta;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secundaria);
        ArrayList <Jugador> miArray= Comunicador.getArray();
        VistaPropia jugadores[]= new VistaPropia[miArray.size()];

        LinearLayout campo1= (LinearLayout) findViewById(R.id.campo1);
        LinearLayout campo2= (LinearLayout) findViewById(R.id.campo2);

        LinearLayout campo1_def= (LinearLayout) campo1.findViewById(R.id.def);
        LinearLayout campo1_del= (LinearLayout) campo1.findViewById(R.id.del);
        LinearLayout campo2_def= (LinearLayout) campo2.findViewById(R.id.def);
        LinearLayout campo2_del= (LinearLayout) campo2.findViewById(R.id.del);

        int mitad= miArray.size()/2;
        int linea=1;
        int linea2=1;
        for (int i = 0; i < jugadores.length; i++) {
            jugadores[i] = new VistaPropia(this);
            jugadores[i].setText(miArray.get(i).getNombre());
            jugadores[i].setImage(miArray.get(i).getFoto());
            if (i < mitad) {
                jugadores[i].setColor(2);
                if (linea <= 3) {
                    campo1_def.addView(jugadores[i]);
                    linea++;
                } else
                    campo1_del.addView(jugadores[i]);
            } else {
                jugadores[i].setColor(1);
                if (linea2 <= 3) {
                    campo2_del.addView(jugadores[i]);
                    linea2++;
                } else {
                    campo2_def.addView(jugadores[i]);
                }
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_secundaria, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
